﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_RPPOON.Zadatak_1_2
{
    class Example_1_2:IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        static void Print(double[] data)
        {
            foreach (var element in data)
            {
                Console.WriteLine(Math.Round(element, 2).ToString());
            }
        }
        
        public void Run()
        {
            Dataset dataset = new Dataset();
            dataset.LoadDataFromCSV("C:\\Users\\Vlatka\\source\\repos\\LV4_RPPOON\\LV4_RPPOON\\bin\\Debug\\netcoreapp3.1\\LV4-Zad1_2.txt");
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            IAnalytics analytics = new Adapter(analyzer);

            double[] perRowAverage = analytics.CalculateAveragePerRow(dataset);
            double[] perColumnAverage = analytics.CalculateAveragePerColumn(dataset);

            Console.WriteLine("Average per row: ");
            Print(perRowAverage);
            Console.WriteLine("Average per column: ");
            Print(perColumnAverage);
        }

    }
}
