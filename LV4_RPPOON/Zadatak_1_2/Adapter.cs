﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_RPPOON.Zadatak_1_2
{
    class Adapter: IAnalytics
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        private double[][] ConvertData(Dataset dataset)
        {
            int rowCount = dataset.GetData().Count;

            double[][] tempData = new double[rowCount][];

            int columnCount = 0;
            foreach (var data in dataset.GetData())
            {
                columnCount = 0;
                foreach (var element in data)
                {
                    columnCount++;
                }
            }

            int i, j;
            for (i = 0; i < rowCount; i++)
            {
                tempData[i] = new double[columnCount];
            }

            i = 0;
            foreach (var data in dataset.GetData())
            {
                j = 0;
                foreach (var element in data)
                {
                    tempData[i][j] = element;
                    j++;
                }
                i++;
            }

            return tempData;
        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}
