﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_RPPOON.Zadatak_6_7
{
    class Example_7 : IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }

        static void Print(bool pass, bool mail)
        {
            if (mail) { Console.WriteLine("Valid e-mail."); }
            else { Console.WriteLine("Invalid e-mail!!"); }

            if (pass) { Console.WriteLine("Valid password."); }
            else { Console.WriteLine("Invalid password!!"); }
        }

        public void Run()
        {
            RegistrationValidator registrationValidator = new RegistrationValidator();


            while (!registrationValidator.IsUserEntryValid(UserEntry.ReadUserFromConsole())) 
            {
                Console.WriteLine("Wrong email or password!!");
                Console.WriteLine("Try again.");
            }

            Console.WriteLine("Loading...");
            System.Threading.Thread.Sleep(2000);
            Console.WriteLine("Welcome back! :)");

        }
    }
}
