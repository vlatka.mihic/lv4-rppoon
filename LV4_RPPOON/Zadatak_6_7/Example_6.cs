﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_RPPOON.Zadatak_6_7
{
    class Example_6: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }

        static void Print(bool pass, bool mail)
        {
            if (mail) { Console.WriteLine("Valid e-mail."); }
            else { Console.WriteLine("Invalid e-mail!!"); }

            if (pass) { Console.WriteLine("Valid password."); }
            else { Console.WriteLine("Invalid password!!"); }
        }

        public void Run()
        {
            String password = "Y8JZjTpE";
            String e_mail = "skassif@gmail.com";

            PasswordValidator passwordValidator = new PasswordValidator(6);
            EmailValidator emailValidator = new EmailValidator();

            bool passworIsValid = passwordValidator.IsValidPassword(password);
            bool emailIsValid = emailValidator.IsValidAddress(e_mail);

            Print(passworIsValid, emailIsValid);
        }
    }
}
