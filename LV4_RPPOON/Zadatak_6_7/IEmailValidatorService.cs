﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_RPPOON.Zadatak_6_7
{
    interface IEmailValidatorService
    {
        bool IsValidAddress(String candidate);
    }
}
