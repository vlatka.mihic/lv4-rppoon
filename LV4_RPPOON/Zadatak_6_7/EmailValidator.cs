﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_RPPOON.Zadatak_6_7
{
    class EmailValidator : IEmailValidatorService
    {
        public bool IsValidAddress(String candidate)
        {
            return ContainsAtSign(candidate) && EndsWithComOrHr(candidate);
        }
        public bool ContainsAtSign(String candidate)
        {
            return candidate.Contains('@');
        }
        public bool EndsWithComOrHr(String candidate)
        {
            if(candidate.EndsWith(".com") || candidate.EndsWith(".hr"))
            {
                return true;
            }
            return false;
        }
    }
}
