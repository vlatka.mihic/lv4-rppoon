﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;


namespace LV4_RPPOON
{
    interface IExample
    {
        System.String Name { get; }
        void Run();

    }
}
