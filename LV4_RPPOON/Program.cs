﻿using System;
using LV4_RPPOON.Zadatak_1_2;
using LV4_RPPOON.Zadatak_3_4_5;
using LV4_RPPOON.Zadatak_6_7;
using System.Collections.Generic;

namespace LV4_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IExample> examples = new List<IExample>()
            {
                new Example_1_2(),
                new Example_3(),
                new Example_4(),
                new Example_5(),
                new Example_6(),
                new Example_7()
            };

            foreach (IExample example in examples)
            {
                PrintUtilities.PrintStart(example.Name);
                example.Run();
                PrintUtilities.PrintEnd();
            }

        }
    }
}
