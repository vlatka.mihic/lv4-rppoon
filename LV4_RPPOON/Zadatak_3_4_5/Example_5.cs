﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_RPPOON.Zadatak_3_4_5
{
    class Example_5: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }

        public void Run()
        {
            Video video = new Video("Spy");
            Book book = new Book("The Fault in Our Stars");
            HotItem hotVideo = new HotItem(video);
            HotItem hotBook = new HotItem(book);

            List<IRentable> rentables = new List<IRentable>()
            {
                new Book("Lovac u žitu"),
                new Video("Real (2013 movie)"),
                hotBook,
                hotVideo
            };

            List<IRentable> flashSale = new List<IRentable>();
            flashSale.Add(new DiscountedItem(rentables[0], 0.6));
            flashSale.Add(new DiscountedItem(rentables[1], 0.6));
            flashSale.Add(new DiscountedItem(rentables[2], 0.6));
            flashSale.Add(new DiscountedItem(rentables[3], 0.6));

            RentingConsolePrinter rentingConsolePrinter = new RentingConsolePrinter();

            rentingConsolePrinter.DisplayItems(flashSale);
            rentingConsolePrinter.PrintTotalPrice(flashSale);
        }
    }
}
