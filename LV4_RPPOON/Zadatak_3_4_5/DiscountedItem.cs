﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_RPPOON.Zadatak_3_4_5
{
    class DiscountedItem : RentableDecorator
    {
        private readonly double DiscountedItemDiscount;
        public DiscountedItem(IRentable rentable, double discount) : base(rentable) 
        {
            this.DiscountedItemDiscount = discount;
        }
        public override double CalculatePrice()
        {
            return (1 - DiscountedItemDiscount) * base.CalculatePrice();
        }
        public override String Description
        {
            get
            {
                return base.Description + string.Format(" now at {0}% off!", (100 * DiscountedItemDiscount).ToString()) ;
            }
        }
    }
}
