﻿using System;
using System.Collections.Generic;
using System.Text;
/*
 U listu iz prošlog zadatka dodati po jednu hit knjigu i jedan hit film i ponoviti ispis. Uočavate li ikakve razlike?
 
    Jedina razlika je u dodanom ponašanju kod hotVideo i hotBook gdje smo naglasili da su u trendu i povećali im cijenu.      
*/


namespace LV4_RPPOON.Zadatak_3_4_5
{
    class Example_4: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }

        public void Run()
        {
            Video video = new Video("Spy");
            Book book = new Book("The Fault in Our Stars");
            HotItem hotVideo = new HotItem(video);
            HotItem hotBook = new HotItem(book);

            List<IRentable> rentables = new List<IRentable>()
            {
                new Book("Lovac u žitu"),
                new Video("Real (2013 movie)"),
                hotBook,
                hotVideo
            };

            RentingConsolePrinter rentingConsolePrinter = new RentingConsolePrinter();

            rentingConsolePrinter.DisplayItems(rentables);
            rentingConsolePrinter.PrintTotalPrice(rentables);
        }
    }
}
