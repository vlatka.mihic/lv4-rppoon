﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_RPPOON.Zadatak_3_4_5
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();
    }
}
