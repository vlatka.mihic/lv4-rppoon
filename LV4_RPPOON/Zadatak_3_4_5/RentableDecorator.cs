﻿using LV4_RPPOON.Zadatak_3_4_5;
using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_RPPOON.Zadatak_3_4_5
{
    abstract class RentableDecorator: IRentable
    {
        private IRentable rentable;
        public RentableDecorator(IRentable rentable)
        {
            this.rentable = rentable;
        }
        public virtual double CalculatePrice()
        {
            return rentable.CalculatePrice();
        }
        public virtual String Description
        {
            get
            {
                return rentable.Description;
            }
        }
    }
}
