﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV4_RPPOON.Zadatak_3_4_5
{
    class RentingConsolePrinter
    {
        public void PrintTotalPrice(List<IRentable> items)
        {
            Console.WriteLine(Math.Round(items.Sum(r => r.CalculatePrice()), 2));
        }
        public void DisplayItems(List<IRentable> items)
        {
            items.ForEach(r => Console.WriteLine(r.Description));
        }
    }
}
