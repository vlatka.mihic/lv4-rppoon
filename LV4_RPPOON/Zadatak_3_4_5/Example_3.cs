﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_RPPOON.Zadatak_3_4_5
{
    class Example_3: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        static void Print(double[] data)
        {
            foreach (var element in data)
            {
                Console.WriteLine(element.ToString());
            }
        }

        public void Run()
        {
            List<IRentable> rentables = new List<IRentable>()
            {
                new Book("Lovac u žitu"),
                new Video("Real (2013 movie)")
            };

            RentingConsolePrinter rentingConsolePrinter = new RentingConsolePrinter();

            rentingConsolePrinter.DisplayItems(rentables);
            rentingConsolePrinter.PrintTotalPrice(rentables);
        }
    }
}
